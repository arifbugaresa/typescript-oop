class Hewan6 {
    name: string = '';

    bernafas() {
        console.log("Hewan sedang bernafas");
    }
}

class Katak6 {
    bernafas() {
        console.log("Katak Bernafas");
    }
}

const katak6 = new Katak6();
katak6.bernafas();